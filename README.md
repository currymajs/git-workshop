# README #

### Forking Repositories ###

* Make a copy of another repository and save it as your own repository
* Click the fork-link
* Give the new repository a name
* Done!

### Setting up an Upstream ###
* Add an upstream repository for the original repository that you have forked
* This is to keep your fork synced with the original
* `git remote -v` to see what remote repositories that your local repository is connected to
* `git remote add upstream #LINK#` to add a new remote upstream repository
    * The link is the same that you use to clone
* When you want to get new changes from the original repository, use `git merge upstream/#BRANCH NAME#`

### Merge Conflicts - What are they? ###

* When a file is changed before pulling new changes
* What changes should be kept?
* When the conflict has occurred, the file in question will be changed to show all changes
* Change the file to how you want it to be
* Commit and push
* Done!

                     Commit #2.1
                    /           \
           Commit #1             Merged commit
                    \           /
                     Commit #2.2

* Try changing Conflict.java from two different places and simulate a merge conflict!
* Merge conflicts pretty much only occur when the same lines have been changed
* Git is smart enough to see if different parts of the same file has been changed and automatically merge the changes